package hello;

// on line change

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


// Starting class (3-4)


public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Ted")
   public void testGreeterTed() 

   {
      g.setName("Austin");
      assertEquals(g.getName(),"Austin");
      assertEquals(g.sayHello(),"Hello Austin!");
   }


   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

    @Test
    @DisplayName("Test for name= 'Nelson'")
    public void testGreeterNelson()
    {
        g.setName("Nelson");
        assertEquals(g.getName(), "Nelson");
        assertEquals(g.sayHello(),"Hello Nelson!");
        assertTrue(g.sayHello().endsWith("!"), "Lock in, before i tweak aht?"); 
        
       
    }

   @Test
   @DisplayName("Test for Name = 'Libin' ")
   public void testGreeterLibin()
   {
       g.setName("Promethean Achilles");
       assertFalse(g.getName().equals("Libin"), "You are not the true owner of this test.\n You shall not pass. ");
       assertNotSame(g.sayHello(),1,"One or nothing");
   }

   @Test
   @DisplayName("Test for Name='Avery'") //added test for A9 JUnit testing
   public void testGreeterAvery() 
   {

      g.setName("Avery");
      assertEquals(g.getName(),"Avery");
      assertFalse(g.sayHello().equals("I'm afraid I can't do that, Avery."));
   }

  



   @Test
   @DisplayName("Test for arv107_A14 objects not same") //added test for A9 JUnit testing
   public void testGreeter_arv107_A14() 
   {

      g.setName("arv107");
	  Greeter h = new Greeter("arv107");
	  assertNotSame(g, h, "How did we get here?");
   }

   @Test
   @DisplayName("Test for Name='Naomi'")
   public void testGreeterNaomi() 
   {
       String name = "Naomi";
       
      g.setName("Naomi");
      assertEquals(g.getName(),"Naomi");
      assertEquals(g.sayHello(),"Hello Naomi!");
      assertFalse(g.sayHello().endsWith("i"), "Can the real Naomi please stand up?"); 
      
      assertSame(g.getName(), name);
   }

   @Test
   @DisplayName("Test for acs190_A14 objects not the same") // Added test for 14 Pull Request

    public void testGreeterAustin()
    {
		g.setName("Austin");
        Greeter h = new Greeter(" Idk if this is where we are suppose to be");
		assertNotSame(g, h, "I told you it was the other way");
	}
}
